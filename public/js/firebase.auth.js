var config = {
    apiKey: "AIzaSyDZIXIdlZ8IRjlCC2Pb8xo1y_bNesvSb8g",
    authDomain: "laporin-cmi.firebaseapp.com",
    databaseURL: "https://laporin-cmi.firebaseio.com",
    projectId: "laporin-cmi",
    storageBucket: "laporin-cmi.appspot.com",
    messagingSenderId: "303329537504"
};

var allowedRole = ["ksi", "cmt", "opr"];
var privilegedRole = ["ksi", "cmt"];

var test;

firebase.initializeApp(config);

firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
        console.log(user)
        getUserRole(user.uid).then(function (role) {
            if (allowedRole.indexOf(role) >= 0) {
                if (window.location.pathname === "/login") {
                    window.location.assign("/");
                } else {
                    $("#userName2").innerHTML = user.displayName;
                    $("#roleInput").val(role);
                    if (user.email == "operator@cimahikota.go.id") {
                        $("#userName").html("PakOP");
                        $('.opr, #verified, #rejected').removeClass('d-none')
                        $('.opr, #verified, #rejected').css('display', 'inline')
                    } else {
                        $("#userName").html(user.displayName)
                        if (user.email == "camat@cimahikota.go.id") {
                            $('.cmt').removeClass('d-none')
                            $('.cmt').css('display', 'inline')
                        } else {
                            $('.ksi').removeClass('d-none')
                            $('.ksi').css('display', 'inline')
                        }
                    }
                    // $(".req-privileges").each(function() {
                    //     $(this).attr("disabled", privilegedRole.indexOf(role) >= 0 ? false : true);
                    // });
                }
            } else {
                firebase
                    .auth()
                    .signOut()
                    .then(function () {
                        if (window.location.pathname === "/login") {
                            //M.toast({html: "You\'re not an authorized admin"})
                        } else {
                            window.location.assign("/login");
                        }
                    })
                    .catch(function (error) {
                        console.error(error.message);
                    });
            }
        });
    } else {
        $("#sidebarMenu").hide();
        if (window.location.pathname !== "/login") {
            window.location.assign("/login");
        }
    }
});

function isAdmin(userID) {
    return firebase
        .database()
        .ref("/fr_kb_authorized_admin/" + userID)
        .once("value")
        .then(function (snaps) {
            var isAdmin = snaps.val() || false;
            return isAdmin;
        })
        .catch(function (error) {
            console.error("Error " + error.code + ": " + error.message);
        });
}

/**
 *
 * @param {string} uid
 * @returns Promise
 */
function getUserRole(uid) {
    var db = firebase.firestore();
    db.settings({
        timestampsInSnapshots: true
    });

    var userRef = db.collection("userData").doc(uid);
    return userRef
        .get()
        .then(function (doc) {
            return doc.data().role;
        })
        .catch(function (error) {
            console.error("Error " + error.code + ": " + error.message);
        });
}

$("#loginForm").on("submit", function (ev) {
    ev.preventDefault();
    var email = $("#email").val();
    var password = $("#password").val();

    firebase
        .auth()
        .signInWithEmailAndPassword(email, password)
        .then(function (success) {
            $("#loginMessage").text("Login Success, redirecting...");
        })
        .catch(function (error) {
            var errorMessage = error.message;
            $("#loginMessage").text(errorMessage);
        });
    // return false
});

// $("#googleSignInbtn").on("click", function() {
//     var provider = new firebase.auth.GoogleAuthProvider();
//     firebase.auth().signInWithRedirect(provider);
// });

function logout() {
    firebase
        .auth()
        .signOut()
        .then(function () {
            window.location.assign("/login");
        })
        .catch(function (error) {
            console.error(error.message);
        });
}
