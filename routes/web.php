<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/login', function () {
    return view('login');
});

Route::get('/data-laporan/{type}', 'APICall@get_all_reports_single');
Route::get('/data-laporan/{role}/{type}', 'APICall@get_all_reports');

Route::get('/data-laporan/edit/{roleInput}/{id}', 'APICall@edit_report_status');

Route::post('/print', 'APICall@makePDF');

Route::get('/data-fasilitas', 'APICall@get_report_category');

Route::get('/data-berita', 'APICall@read_all_news_article');