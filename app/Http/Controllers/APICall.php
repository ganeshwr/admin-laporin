<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use PDF;

class APICall extends Controller
{   
    // GET
    public function get_all_reports_single($type){
        $client = new Client(); 
        $res = $client->get('https://asia-northeast1-laporin-cmi.cloudfunctions.net/getReports');
        $val = $res->getBody();

        $obj = json_decode($val);
        
        return view('data_laporan.'.$type)->with('obj', $obj);
    }
    
    public function get_all_reports($role ,$type){
        $client = new Client(); 
        $res = $client->get('https://asia-northeast1-laporin-cmi.cloudfunctions.net/getReports');
        $val = $res->getBody();

        $obj = json_decode($val);
        
        return view('data_laporan.'.$role.'_'.$type)->with('obj', $obj);
    }

    public function get_report_category(){
        $client = new Client(); 
        $res = $client->get('https://asia-northeast1-laporin-cmi.cloudfunctions.net/getReportCategory');
        $val = $res->getBody();

        $obj = json_decode($val);
        
        return view('data_fasilitas')->with('obj', $obj);
    }

    public function read_all_news_article(){
        $client = new Client(); 
        $res = $client->get('https://asia-northeast1-laporin-cmi.cloudfunctions.net/news/readAll');
        $val = $res->getBody();

        $obj = json_decode($val);
        
        return view('data_berita')->with('obj', $obj);
    }

    public function edit_report_status($roleInput ,$id){
        $client = new Client(); 
        $res = $client->put('https://asia-northeast1-laporin-cmi.cloudfunctions.net/report/'.$id, [
            'form_params' => [
                'report_category_id' => $id,
                'role' => $roleInput,
                'status' => 'APPROVAL'
            ]
        ]);

        return back();
    }

    // makePDF
    public function makePDF(Request $request){
        $client = new Client(); 
        $res = $client->get('https://asia-northeast1-laporin-cmi.cloudfunctions.net/report/'.$request->id);
        $val = $res->getBody();
        $obj = json_decode($val);

        $months = ["", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];
        
        // POST Data
        $data['nomor_surat'] = $request->nomor_surat;
        $data['tanggal_pengecekan'] = $request->tanggal_pengecekan;
        $data['kelurahan'] = $request->kelurahan;
        // $data['foto_pemeriksaan'] = $request->file('foto_pemeriksaan');

        $date = date('d')." ".$months[date('m')]." ".date('Y');

        $data['date'] = $date;
        if($obj->data->report_category_id == 'DLH_Trash'){
            $data['kepala'] = 'Dinas Lingkungan Hidup cq. UPT Pelayanan Persampahan Kota Cimahi';
            $data['body_1'] = 'telah terjadi penumpukan sampah dengan rincian data sebagai berikut :';
            $data['body_2'] = 'Untuk itu kami mohon bantuan dari Dinas Lingkungan Hidup Kota Cimahi melalui UPT Pelayanan Persampahan kota Cimahi untuk melakukan pemeriksaan lapangan dan pengaturan pengangkutan sampah di lokasi tersebut.';

            $data['tipe_deskripsi'] = 'Penumpukan';
            $data['deskripsi'] = $obj->data->report_detail;
            $data['lokasi'] = $obj->data->report_location;
            $data['titik_koordinat'] = $obj->data->report_latlong;

        }
        elseif ($obj->data->report_category_id == 'DLH_Trash_Sewer') {
            $data['kepala'] = 'Dinas Lingkungan Hidup cq. UPT Pelayanan Persampahan Kota Cimahi';
            $data['body_1'] = 'telah terjadi penumpukan sampah di badan sungai dengan rincian data sebagai berikut :';
            $data['body_2'] = 'Untuk itu kami mohon bantuan dari Dinas Lingkungan Hidup Kota Cimahi melalui UPT Pelayanan Persampahan kota Cimahi untuk melakukan pemeriksaan lapangan dan pengaturan pengangkutan sampah di lokasi tersebut.';

            $data['tipe_deskripsi'] = 'Penumpukan';
            $data['deskripsi'] = $obj->data->report_detail;
            $data['lokasi'] = $obj->data->report_location;
            $data['titik_koordinat'] = $obj->data->report_latlong;
        }
        elseif ($obj->data->report_category_id == 'DLH_Tree') {
            $data['kepala'] = 'Dinas Lingkungan Hidup Kota Cimahi';
            $data['body_1'] = 'ditemukan adanya pohon dengan kondisi sebagai berikut :';
            $data['body_2'] = 'Untuk itu kami mohon bantuan dari Dinas Lingkungan Hidup Kota Cimahi untuk melakukan pemeriksaan lapangan dan tindakan lanjutan yang dianggap perlu guna mencegah hal – hal yang merugikan dikemudian hari.';

            $data['tipe_deskripsi'] = 'Kondisi';
            $data['deskripsi'] = $obj->data->report_detail;
            $data['lokasi'] = $obj->data->report_location;
            $data['titik_koordinat'] = $obj->data->report_latlong;
        }
        elseif ($obj->data->report_category_id == 'DPKP_Pathway') {
            $data['kepala'] = 'Dinas Perumahan dan Kawasan Permukiman Kota Cimahi';
            $data['body_1'] = 'ditemukan adanya kerusakan prasarana dan sarana umum (PSU) sebagai berikut :';
            $data['body_2'] = 'Untuk itu kami mohon bantuan dari Dinas Perumahan dan Kawasan Permukiman Kota Cimahi untuk melakukan pemeriksaan lapangan dan tindakan perbaikan atas kerusakan prasarana dan sarana umum (PSU) tersebut.';

            $data['tipe_deskripsi'] = 'Kerusakan';
            $data['deskripsi'] = $obj->data->report_detail;
            $data['lokasi'] = $obj->data->report_location;
            $data['titik_koordinat'] = $obj->data->report_latlong;
        }
        elseif ($obj->data->report_category_id == 'DPKP_Water') {
            $data['kepala'] = 'Dinas Perumahan dan Kawasan Permukiman Kota Cimahi';
            $data['body_1'] = 'ditemukan adanya kerusakan prasarana dan sarana umum (PSU) sebagai berikut :';
            $data['body_2'] = 'Untuk itu kami mohon bantuan dari Dinas Perumahan dan Kawasan Permukiman Kota Cimahi untuk melakukan pemeriksaan lapangan dan tindakan perbaikan atas kerusakan prasarana dan sarana umum (PSU) tersebut.';

            $data['tipe_deskripsi'] = 'Kerusakan';
            $data['deskripsi'] = $obj->data->report_detail;
            $data['lokasi'] = $obj->data->report_location;
            $data['titik_koordinat'] = $obj->data->report_latlong;
        }
        else {
            $data['kepala'] = 'Dinas Perhubungan Kota Cimahi';
            $data['body_1'] = 'ditemukan adanya kerusakan prasarana dan sarana umum (PSU) sebagai berikut :';
            $data['body_2'] = 'Untuk itu kami mohon bantuan dari Dinas Perhubungan Kota Cimahi untuk melakukan pemeriksaan lapangan dan tindakan perbaikan atas kerusakan prasarana dan sarana umum (PSU) tersebut.';

            $data['tipe_deskripsi'] = 'Kerusakan';
            $data['deskripsi'] = $obj->data->report_detail;
            $data['lokasi'] = $obj->data->report_location;
            $data['titik_koordinat'] = $obj->data->report_latlong;
        }
        $data['pelapor'] = $obj->data->reporter_name;
        $tanggal_laporan = date_parse($obj->data->created_timestamp);
        $data['tanggal_laporan'] = $tanggal_laporan['day'].' '.$months[$tanggal_laporan['month']].' '.$tanggal_laporan['year'];
        $data['longlat'] = "https://www.mapquestapi.com/staticmap/v4/getplacemap?key=0xfmikwALbePdV65QX5koF2Wbw1iWHjD&location=".$data['titik_koordinat']."&size=311,198&type=map&zoom=16&imagetype=png&scalebar=false&showicon=bluegreen_1-1";
        $data['foto_pemeriksaan'] = self::encode_img_base64($request->file('foto_pemeriksaan'), 'png');
        $data['foto_pengaduan'] = "data:image/png;base64,".str_replace ("\n", "", base64_encode(file_get_contents($obj->data->report_img_url)));
        
        $pdf = PDF::loadView('layout.surat', $data)->setPaper(array(0, 0, 465, 710), 'portrait');
        return $pdf->stream();
    }

    public function encode_img_base64($img_path = false, $img_type = 'png'){
        if( $img_path ){
            //convert image into Binary data
            $img_data = fopen($img_path, 'rb');
            $img_size = filesize ($img_path);
            $binary_image = fread ($img_data, $img_size);
            fclose ($img_data);
    
            //Build the src string to place inside your img tag
            $img_src = "data:image/".$img_type.";base64,".str_replace ("\n", "", base64_encode ( $binary_image ) );
            return $img_src;
        }
        return false;
    }
}
