@extends('layout.app')

@section('breadcrumb')
<li class="breadcrumb-item active">Data Laporan</li>
<li class="breadcrumb-item active">Verified</li>
@endsection

@section('content')
<div class="container-fluid mb-3">
    <div class="animated fadeIn">
        <h3>Data Laporan - Verified</h3>
        <table id="dataLaporan" class="table table-striped table-bordered" style="width:100%">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>NIK</th>
                    <th>Subject</th>
                    <th>Message</th>
                    <th>Location</th>
                    <th>Lat/Long</th>
                    <th>Category ID</th>
                    <th class="image-col">Image</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @for ($i = 0; $i < sizeof($obj->data); $i++)
                    @if ($obj->data[$i]->status->opr > 0 && $obj->data[$i]->status->ksi > 0 && $obj->data[$i]->status->cmt > 0)
                    <tr>
                        <td>{{$i+1}}</td>
                        <td>{{property_exists($obj->data[$i], 'reporter_nik') ? $obj->data[$i]->reporter_nik : 'NULL'}}</td>
                        <td>{{$obj->data[$i]->report_title}}</td>
                        <td>{{ str_limit($obj->data[$i]->report_detail, $limit = 100, $end = '...') }}</td>
                        <td>{{$obj->data[$i]->report_location}}</td>
                        <td>{{$obj->data[$i]->report_latlong}}</td>
                        <td>{{$obj->data[$i]->report_category_id}}</td>
                        <td class="text-center"><img src="{{$obj->data[$i]->report_img_url}}" alt="" class="img-thumbnail image-laporan"></td>
                        <td>
                            <button type="button" class="btn btn-dark btn-block" data-toggle="modal" data-target="#printLaporan{{$i}}">PRINT</button>
                            <!-- Modal Tambah Fasilitas -->
                            <div class="modal fade" id="printLaporan{{$i}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
                                aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalCenterTitle">Print Laporan</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <form action="{{url('/print')}}" method="POST" target="_blank" enctype="multipart/form-data">
                                            @csrf
                                            <input name="id" type="hidden" class="form-control" id="id" value="{{$obj->data[$i]->report_id}}">
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <label for="nomor_surat">Nomor Surat</label>
                                                    <input name="nomor_surat" type="text" class="form-control" id="nomor_surat"
                                                        placeholder="cth: 48759" required>
                                                </div>
                                                <div class="form-group">
                                                    <label for="tanggal_pengecekan">Tanggal Pengecekan</label>
                                                    <div class="input-group">
                                                        <input name="tanggal_pengecekan" id="tanggal_pengecekan"
                                                            data-toggle="datepicker" type="text" class="form-control docs-date"
                                                            placeholder="Pick a date" autocomplete="off" required>
                                                        <div class="input-group-append">
                                                            <button type="button" class="btn btn-outline-secondary docs-datepicker-trigger"
                                                                disabled>
                                                                <i class="fa fa-calendar" aria-hidden="true"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="kelurahan">Kelurahan</label>
                                                    <select name="kelurahan" id="kelurahan" class="form-control">
                                                        <optgroup label="Cimahi Selatan">
                                                            <option value="Cibeber">Cibeber</option>
                                                            <option value="Cibeureum">Cibeureum</option>
                                                            <option value="Leuwigajah">Leuwigajah</option>
                                                            <option value="Melong">Melong</option>
                                                            <option value="Utama">Utama</option>
                                                        </optgroup>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label for="foto_pemeriksaan">Foto Pemeriksaan Petugas</label>
                                                    <div class="custom-file">
                                                        <input name="foto_pemeriksaan" type="file" class="custom-file-input"
                                                            id="foto_pemeriksaan" accept="image/*" required>
                                                        <label class="custom-file-label" for="foto_pemeriksaan">Choose
                                                            Image</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary">Save
                                                    changes</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    @endif
                    @endfor
            </tbody>
            <tfoot>
                <tr>
                    <th>ID</th>
                    <th>NIK</th>
                    <th>Subject</th>
                    <th>Message</th>
                    <th>Location</th>
                    <th>Lat/Long</th>
                    <th>Category ID</th>
                    <th>Image</th>
                    <th>Action</th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>
<input type="hidden" name="ll" id="roleInput">
@endsection

@section('script')
<script>
    function printLaporan(nomor_surat, tanggal_pengecekan, foto_pemeriksaan, id) {
        window.location = `/print/${nomor_surat}/${tanggal_pengecekan}/${foto_pemeriksaan}/${id}`
    }

    var date = $('[data-toggle="datepicker"]').datepicker({
        format: 'dd-mm-yy',
        days: ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'],
        months: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September',
            'Oktober', 'November', 'Desember'
        ],
    });

    $('[data-toggle="datepicker"]').change(function (e) {
        day = date.datepicker('getDate').getDate();
        month = date.datepicker('getMonthName');
        year = date.datepicker('getDate').getFullYear();

        $(this).val(`${day} ${month} ${year}`)
    });

    $('#foto_pemeriksaan').on('change', function () {
        path = $(this).val();
        path = path.substring(path.lastIndexOf("\\") + 1, 100);

        $(this).next().html(path);
    })

</script>
@endsection
