@extends('layout.app')

@section('breadcrumb')
<li class="breadcrumb-item active">Data Laporan</li>
<li class="breadcrumb-item active">Rejected</li>
@endsection

@section('content')
<div class="container-fluid mb-3">
    <div class="animated fadeIn">
        <h3>Data Laporan - Rejected</h3>
        <table id="dataLaporan" class="table table-striped table-bordered" style="width:100%">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>NIK</th>
                    <th>Subject</th>
                    <th>Message</th>
                    <th>Location</th>
                    <th>Lat/Long</th>
                    <th>Category ID</th>
                    <th class="image-col">Image</th>
                </tr>
            </thead>
            <tbody>
                @for ($i = 0; $i < sizeof($obj->data); $i++)
                    @if ($obj->data[$i]->status->opr < 0 && $obj->data[$i]->status->ksi < 0 &&
                    $obj->data[$i]->status->cmt < 0)
                    <tr>
                        <td>{{$i+1}}</td>
                        <td>{{property_exists($obj->data[$i], 'reporter_nik') ? $obj->data[$i]->reporter_nik : 'NULL'}}</td>
                        <td>{{$obj->data[$i]->report_title}}</td>
                        <td>{{ str_limit($obj->data[$i]->report_detail, $limit = 100, $end = '...') }}</td>
                        <td>{{$obj->data[$i]->report_location}}</td>
                        <td>{{$obj->data[$i]->report_latlong}}</td>
                        <td>{{$obj->data[$i]->report_category_id}}</td>
                        <td class="text-center"><img src="{{$obj->data[$i]->report_img_url}}" alt="" class="img-thumbnail image-laporan"></td>
                    </tr>
                    @endif
                    @endfor
            </tbody>
            <tfoot>
                <tr>
                    <th>ID</th>
                    <th>NIK</th>
                    <th>Subject</th>
                    <th>Message</th>
                    <th>Location</th>
                    <th>Lat/Long</th>
                    <th>Category ID</th>
                    <th>Image</th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>
<input type="hidden" name="ll" id="roleInput">
@endsection
