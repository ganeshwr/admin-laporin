@extends('layout.app')

@section('breadcrumb')
<li class="breadcrumb-item active">Data Laporan</li>
<li class="breadcrumb-item active">Unverified</li>
@endsection

@section('content')
<input type="hidden" name="ll" id="roleInput">
<div class="container-fluid mb-3">
    <div class="animated fadeIn">
        <h3>Data Laporan - Unverified</h3>
        <table id="dataLaporan" class="table table-striped table-bordered" style="width:100%">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>NIK</th>
                    <th>Subject</th>
                    <th>Message</th>
                    <th>Location</th>
                    <th>Lat/Long</th>
                    <th>Category ID</th>
                    <th class="image-col">Image</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @for ($i = 0; $i < sizeof($obj->data); $i++)
                    @if ($obj->data[$i]->status->cmt == 0)
                    <tr>
                        <td>{{$i+1}}</td>
                        <td>{{property_exists($obj->data[$i], 'reporter_nik') ? $obj->data[$i]->reporter_nik : 'NULL'}}</td>
                        <td>{{$obj->data[$i]->report_title}}</td>
                        <td>{{ str_limit($obj->data[$i]->report_detail, $limit = 100, $end = '...') }}</td>
                        <td>{{$obj->data[$i]->report_location}}</td>
                        <td>{{$obj->data[$i]->report_latlong}}</td>
                        <td>{{$obj->data[$i]->report_category_id}}</td>
                        <td class="text-center"><img src="{{$obj->data[$i]->report_img_url}}" alt="" class="img-thumbnail image-laporan"></td>
                        <td>
                            <button type="button" class="btn btn-outline-primary btn-block req-privileges" onclick="editVerify('{{$obj->data[$i]->report_id}}')">Verify</button>
                            <button type="button" class="btn btn-outline-danger btn-block req-privileges">Reject</button>
                        </td>
                    </tr>

                    @endif
                    @endfor
            </tbody>
            <tfoot>
                <tr>
                    <th>ID</th>
                    <th>NIK</th>
                    <th>Subject</th>
                    <th>Message</th>
                    <th>Location</th>
                    <th>Lat/Long</th>
                    <th>Category ID</th>
                    <th>Image</th>
                    <th>Action</th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>
@endsection
@section('script')
<script>
    function editVerify(id) {
        roleInput = $('#roleInput').val()
        window.location = `/data-laporan/edit/${roleInput}/${id}`
    }

</script>
@endsection
