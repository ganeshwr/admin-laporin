@extends('layout.app')

@section('breadcrumb')
<li class="breadcrumb-item active">Data Berita</li>
@endsection

@section('content')
<div class="container-fluid mb-3">
    <div class="animated fadeIn">
        <h3>Data Berita</h3>
        <!-- Button trigger modal tambah fasilitas -->
        <button type="button" class="btn btn-outline-primary mb-2" data-toggle="modal" data-target="#addBerita">
            <i class="nav-icon icon-plus"></i>
            Tambah Data
        </button>
        <table id="dataLaporan" class="table table-striped table-bordered" style="width:100%">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Title</th>
                    <th>Content</th>
                    <th class="image-col">Thumbnail</th>
                    <th>Created At</th>
                    <th>Updated At</th>
                </tr>
            </thead>
            <tbody>
                @for ($i = 0; $i < sizeof($obj->data); $i++)
                    <tr>
                        <td>{{$obj->data[$i]->id}}</td>
                        <td>{{$obj->data[$i]->news_title}}</td>
                        <td>
                            {{ str_limit($obj->data[$i]->news_content, $limit = 100, $end = '...') }}
                        </td>
                        <td class="text-center"><img src="{{$obj->data[$i]->news_thumbnail_url}}" alt="" class="img-thumbnail image-laporan"></td>
                        <td>{{$obj->data[$i]->created_timestamp}}</td>
                        <td>{{$obj->data[$i]->updated_timestamp}}</td>
                    </tr>
                    @endfor
            </tbody>
            <tfoot>
                <tr>
                    <th>ID</th>
                    <th>Title</th>
                    <th>Content</th>
                    <th class="image-col">Thumbnail</th>
                    <th>Created At</th>
                    <th>Updated At</th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>
@endsection

@section('modal')
<!-- Modal Tambah Fasilitas -->
<div class="modal fade" id="addBerita" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Tambah Berita</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="news_title">News Title</label>
                        <input type="text" class="form-control" id="news_title" placeholder="cth: Pohon Tumbang di Cimahi"
                            required>
                    </div>
                    <div class="form-group">
                        <label for="news_content">News Content</label> <small id="words_remaining">Min 50 words remaining</small>
                        <br>
                        <div class="invalid-tooltip">Please provide a valid content !</div>
                        {{-- <textarea class="form-control" id="news_content" rows="3" placeholder="Pohon yang sudah tidak kuat menahan bebannya..."></textarea>
                        --}}
                        <div id="news_content"></div>
                    </div>
                    <div class="form-group">
                        <label for="news_thumbnail">News Thumbnail</label>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="news_thumbnail" accept="image/*" required>
                            <label class="custom-file-label" for="news_thumbnail">Choose Image</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="news_reporter_uid">News Thumbnail Source</label>
                        <input type="text" class="form-control" id="news_thumbnail_source" placeholder="cth: pexels.com"
                            required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" onclick="submitData()">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    var quill = new Quill('#news_content', {
        debug: 'info',
        placeholder: 'Isi Berita',
        theme: 'snow'
    });

</script>

<script>
    function escapeHtml(text) {
        var map = {
            '&': '&amp;',
            '<': '&lt;',
            '>': '&gt;',
            '"': '&quot;',
            "'": '&#039;'
        };

        return text.replace(/[&<>"']/g, function (m) {
            return map[m];
        });
    }

    function submitData() {
        if (quill.getText().length >= 50) {
            $('.invalid-tooltip').css('display', 'none')
            $('#words_remaining').html('')
            
            var form = new FormData();
            form.append("news_title", $('#news_title').val());
            form.append("news_content", quill.getText());
            form.append("news_content_html", escapeHtml(quill.root.innerHTML));

            form.append("news_thumbnail", $("#news_thumbnail")[0].files[0]);
            form.append("news_thumbnail_source", $('#news_thumbnail_source').val());

            var settings = {
                "async": true,
                "crossDomain": true,
                "url": "https://asia-northeast1-laporin-cmi.cloudfunctions.net/news/write",
                "method": "POST",
                "headers": {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                "processData": false,
                "contentType": false,
                "mimeType": "multipart/form-data",
                "data": form
            }

            $.ajax(settings).done(function (response) {
                console.log(response);
                location.reload();
            });
        } else {
            $('.invalid-tooltip').css('display', 'inline')
            $('.invalid-tooltip').css('position', 'relative')
            $('#words_remaining').html(`Min ${50-(quill.getText().length-1)} words remaining`)
        }
    }

    $('#news_thumbnail').on('change', function () {
        path = $(this).val();
        path = path.substring(path.lastIndexOf("\\") + 1, 100);

        $(this).next().html(path);
    })

</script>
@endsection
