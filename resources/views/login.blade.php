<!DOCTYPE html>

<html lang="en">
    <head>
        <base href="./" />
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no" />
        <meta name="description" content="CoreUI - Open Source Bootstrap Admin Template" />
        <meta name="author" content="Łukasz Holeczek" />
        <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,jQuery,CSS,HTML,RWD,Dashboard" />
        <title>Admin Sikokom</title>

        <link rel="stylesheet" href="{{url('css/coreui-icons.min.css')}}">
        <link rel="stylesheet" href="{{url('css/flag-icon.min.css')}}">
        <link rel="stylesheet" href="{{url('css/font-awesome.min.css')}}">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.css">
        <!-- Main styles for this application-->
        <link rel="stylesheet" href="{{url('css/coreui.min.css')}}">
        <link rel="stylesheet" href="{{url('css/pace.min.css')}}">
        <link rel="stylesheet" href="{{url('css/datatables.min.css')}}">
        <link rel="stylesheet" href="{{url('css/quill.snow.css')}}">
        <link rel="stylesheet" href="{{url('css/style.css')}}">
        <script>
            (function(i, s, o, g, r, a, m) {
                i["GoogleAnalyticsObject"] = r;
                (i[r] =
                    i[r] ||
                    function() {
                        (i[r].q = i[r].q || []).push(arguments);
                    }),
                    (i[r].l = 1 * new Date());
                (a = s.createElement(o)), (m = s.getElementsByTagName(o)[0]);
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m);
            })(window, document, "script", "https://www.google-analytics.com/analytics.js", "ga");
            ga("create", "UA-118965717-1", "auto");
            ga("send", "pageview");
        </script>
    </head>
    <body class="app flex-row align-items-center">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card-group">
                        <div class="card p-4">
                            <div class="card-body">
                                <h1>Login</h1>
                                <p class="text-muted" id="loginMessage">Sign In to your account</p>
                                <form id="loginForm">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"> <i class="icon-user"></i> </span>
                                        </div>
                                        <input class="form-control" type="email" placeholder="Email" id="email" />
                                    </div>
                                    <div class="input-group mb-4">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"> <i class="icon-lock"></i> </span>
                                        </div>
                                        <input class="form-control" type="password" placeholder="Password" id="password" />
                                    </div>
                                    <div class="row">
                                        <div class="col-6"><button class="btn btn-primary px-4" type="submit" id="loginButton">Login</button></div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script src="{{url('js/pace.min.js')}}"></script>
        <script src="{{url('js/perfect-scrollbar.min.js')}}"></script>

        <script src="{{url('js/jquery-3.3.1.min.js')}}"></script>
        <script src="{{url('js/popper.min.js')}}"></script>
        <script src="{{url('js/bootstrap.min.js')}}"></script>
        <script src="{{url('js/coreui.min.js')}}"></script>

        <script src="{{url('js/datatables.min.js')}}"></script>
        <script src="{{url('js/quill.js')}}"></script>

        <script src="{{url('js/custom.js')}}"></script>
        <script src="https://www.gstatic.com/firebasejs/5.5.9/firebase.js"></script>
        <script src="{{url('js/firebase.auth.js')}}"></script>
    </body>
</html>
