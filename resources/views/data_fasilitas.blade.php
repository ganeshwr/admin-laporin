@extends('layout.app')

@section('breadcrumb')
<li class="breadcrumb-item active">Data Fasilitas</li>
@endsection

@section('content')
<div class="container-fluid mb-3">
    <div class="animated fadeIn">
        <h3>Data Fasilitas</h3>
        <!-- Button trigger modal tambah fasilitas -->
        {{-- <button type="button" class="btn btn-outline-primary mb-2" data-toggle="modal" data-target="#addFasilitas">
            <i class="nav-icon icon-plus"></i>
            Tambah Data
        </button> --}}
        <table id="dataLaporan" class="table table-striped table-bordered" style="width:100%">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Category Name</th>
                    <th>Category Detail</th>
                    <th class="image-col">Category Image URL</th>
                </tr>
            </thead>
            <tbody>
                @for ($i = 0; $i < sizeof($obj->data); $i++)
                    <tr>
                        <td>{{$obj->data[$i]->category_id}}</td>
                        <td>{{$obj->data[$i]->category_name}}</td>
                        <td>
                            {{ str_limit($obj->data[$i]->category_detail, $limit = 100, $end = '...') }}
                        </td>
                        <td class="text-center"><img src="{{$obj->data[$i]->category_image_url}}" alt="" class="img-thumbnail image-laporan"></td>
                    </tr>
                    @endfor
            </tbody>
            <tfoot>
                <tr>
                    <td>ID</td>
                    <th>Category Name</th>
                    <th>Category Detail</th>
                    <th class="image-col">Category Image URL</th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>
@endsection

@section('modal')
<!-- Modal Tambah Fasilitas -->
<div class="modal fade" id="addFasilitas" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Tambah Kategori</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{url('/data-fasilitas/add')}}" method="POST">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="category_name">Category Name</label>
                        <input type="text" class="form-control" id="category_name" name="category_name" placeholder="cth: Halte Rusak">
                    </div>
                    <div class="form-group">
                        <label for="category_image_url">Category Image</label>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="category_image_url" name="category_image_url"
                                accept="image/*">
                            <label class="custom-file-label" for="category_image_url">Choose Image</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="category_detail">Category Detail</label>
                        <textarea class="form-control" id="category_detail" name="category_detail" rows="3" placeholder="Pohon yang sudah tidak kuat menahan bebannya..."></textarea>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" onclick="submitData()">Save changes</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    function submitData() {
        var form = $('form')
        formData = new FormData(form);
        formData.append("image", $("#category_image_url")[0].files[0]);

        var settings = {
            "async": true,
            "crossDomain": true,
            "url": "https://asia-northeast1-laporin-cmi.cloudfunctions.net/upload/image/category",
            "method": "POST",
            "headers": {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            },
            "processData": false,
            "contentType": false,
            "mimeType": "multipart/form-data",
            "data": formData
        }

        $.ajax(settings).done(function (response) {
            console.log(response);

            var value = {
                "category_name": $('#category_name').val(),
                "category_image_url": JSON.parse(response).data,
                "category_detail": $('#category_detail').val()
            }

            var settings = {
                "async": true,
                "crossDomain": true,
                "url": "https://asia-northeast1-laporin-cmi.cloudfunctions.net/addNewReportCategory",
                "method": "POST",
                "headers": {
                    "Content-Type": "application/json",
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                "processData": false,
                "data": JSON.stringify(value),
            }

            $.ajax(settings).done(function (response) {
                console.log(response);
                location.reload();
            });
        });
    }

</script>
@endsection
