<html>

<head>
    <meta http-equiv=Content-Type content="text/html; charset=UTF-8">
    <style type="text/css">
        span.cls_002 {
            font-family: Times, serif;
            font-size: 20.1px;
            color: rgb(0, 0, 0);
            font-weight: normal;
            font-style: normal;
            text-decoration: none
        }

        div.cls_002 {
            font-family: Times, serif;
            font-size: 20.1px;
            color: rgb(0, 0, 0);
            font-weight: normal;
            font-style: normal;
            text-decoration: none
        }

        span.cls_003 {
            font-family: Times, serif;
            font-size: 25.1px;
            color: rgb(0, 0, 0);
            font-weight: bold;
            font-style: normal;
            text-decoration: none
        }

        div.cls_003 {
            font-family: Times, serif;
            font-size: 25.1px;
            color: rgb(0, 0, 0);
            font-weight: bold;
            font-style: normal;
            text-decoration: none
        }

        span.cls_004 {
            font-family: Arial, serif;
            font-size: 12.1px;
            color: rgb(0, 0, 0);
            font-weight: normal;
            font-style: normal;
            text-decoration: none
        }

        div.cls_004 {
            font-family: Arial, serif;
            font-size: 12.1px;
            color: rgb(0, 0, 0);
            font-weight: normal;
            font-style: normal;
            text-decoration: none
        }

        span.cls_005 {
            font-family: Arial, serif;
            font-size: 12.1px;
            color: rgb(0, 0, 0);
            font-weight: bold;
            font-style: normal;
            text-decoration: none
        }

        div.cls_005 {
            font-family: Arial, serif;
            font-size: 12.1px;
            color: rgb(0, 0, 0);
            font-weight: bold;
            font-style: normal;
            text-decoration: none
        }

        .page-break {
            page-break-before: always;
        }

    </style>
</head>

<body>
    <div style="position:absolute;left:50%;margin-left:-307px;top:-42px;width:610px;height:936px;border-style:outset;overflow:hidden">
        <div style="position:absolute;left:0px;top:0px">
            <img src="./img/surat/background1.jpg" width=610 height=936></div>
        <div style="position:absolute;left:164.50px;top:26.20px" class="cls_002"><span class="cls_002">PEMERINTAH
                DAERAH KOTA CIMAHI</span></div>
        <div style="position:absolute;left:146.10px;top:54.30px" class="cls_003"><span class="cls_003">KECAMATAN CIMAHI
                SELATAN</span></div>
        <div style="position:absolute;left:204.00px;top:90.30px" class="cls_004"><span class="cls_004">Jl. Baros No. 14
                Telp. (022) 6629676 Cimahi 40533</span></div>
        <div style="position:absolute;left:333.65px;top:138.20px" class="cls_004"><span class="cls_004">Cimahi,
                {{$date}}</span></div>
        <div style="position:absolute;left:368.70px;top:164.00px" class="cls_004"><span class="cls_004">K e p a d a</span></div>
        <div style="position:absolute;left:71.00px;top:183.80px" class="cls_004"><span class="cls_004">Nomor</span></div>
        <div style="position:absolute;left:131.90px;top:183.80px" class="cls_004"><span class="cls_004">:</span></div>
        <div style="position:absolute;left:141.90px;top:183.80px" class="cls_004"><span class="cls_004">490/{{$nomor_surat}}/Sapras</span></div>
        <div style="position:absolute;left:333.57px;top:183.80px" class="cls_004"><span class="cls_004">Yth. </span></div>
        <div style="position:absolute;left:71.00px;top:197.60px" class="cls_004"><span class="cls_004">Sifat</span></div>
        <div style="position:absolute;left:131.90px;top:197.60px" class="cls_004"><span class="cls_004">: Segera</span></div>
        <div style="position:absolute;left:367.00px;top:183.80px" class="cls_004">
            <div style="padding-right: 50px">
                <span class="cls_004">Kepala {{$kepala}}<br>Kota Cimahi <br>di <br>CIMAHI</span>
            </div>
        </div>
        <div style="position:absolute;left:71.00px;top:211.40px" class="cls_004"><span class="cls_004">Lampiran</span></div>
        <div style="position:absolute;left:131.90px;top:211.40px" class="cls_004"><span class="cls_004">:</span></div>
        <div style="position:absolute;left:141.90px;top:211.40px" class="cls_004"><span class="cls_004">1 (Satu) Berkas</span></div>
        <div style="position:absolute;left:71.00px;top:225.20px" class="cls_004"><span class="cls_004">Hal</span></div>
        <div style="position:absolute;left:131.90px;top:225.20px" class="cls_004"><span class="cls_004">: Laporan
                Pengaduan</span></div>
        <div style="position:absolute;left:143.00px;top:290.20px;padding-right:80px;text-align: justify" class="cls_004">
            <span class="cls_004" style="line-height: 20px;">
                <span style="display:inline-block; width: 30px;"></span> Berdasarkan laporan pengaduan masyarakat dari
                Saudara/i {{$pelapor}} pada tanggal {{$tanggal_laporan}} dan hasil pemeriksaan oleh petugas pada
                tanggal {{$tanggal_pengecekan}}

                {{$body_1}}
            </span>
        </div>
        {{-- <div style="position:absolute;left:143.00px;top:395.00px" class="cls_004"><span class="cls_004">Deskripsi
                {{$tipe_deskripsi}}</span></div>
        <div style="position:absolute;left:287.00px;top:395.00px" class="cls_004"><span class="cls_004">:
                {{$deskripsi}} </span></div>
        <div style="position:absolute;left:143.00px;top:415.70px" class="cls_004"><span class="cls_004">Lokasi</span></div>
        <div style="position:absolute;left:287.00px;top:415.70px" class="cls_004"><span class="cls_004">:
                {{$lokasi}} </span></div>
        <div style="position:absolute;left:143.00px;top:436.40px" class="cls_004"><span class="cls_004">Titik Koordinat</span></div>
        <div style="position:absolute;left:287.00px;top:436.40px" class="cls_004"><span class="cls_004">:
                {{$titik_koordinat}} </span></div>
        <div style="position:absolute;left:143.00px;top:457.10px" class="cls_004"><span class="cls_004">Foto</span></div>
        <div style="position:absolute;left:287.00px;top:457.10px" class="cls_004"><span class="cls_004">: Terlampir</span></div>
        --}}
        <div style="position:absolute;left:143.00px;top:395.00px;padding-right:80px;text-align: justify;line-height: 10px;"
            class="cls_004">
            <div style="background-color: red;display: flex;justify-content: center;margin:0;">
                <div>Deskripsi Kerusakan <span style="display:inline-block; width: 20px;"></span></div>
                <div style="margin-left: 135px;line-height: 14px;">: {{$deskripsi}}</div>
            </div>
            <div style="background-color: red;display: flex;justify-content: center;margin:0;">
                <div>Lokasi <span style="display:inline-block; width: 20px;"></span></div>
                <div style="margin-left: 135px;">: {{$lokasi}}</div>
            </div>
            <div style="background-color: red;display: flex;justify-content: center;margin:0">
                <div>Titik Koordinat <span style="display:inline-block; width: 20px;"></span></div>
                <div style="margin-left: 135px;">: {{$titik_koordinat}}</div>
            </div>
            <div style="background-color: red;display: flex;justify-content: center;margin:0">
                <div>Foto <span style="display:inline-block; width: 20px;"></span></div>
                <div style="margin-left: 135px;">: Terlampir</div>
            </div>
            <br>
            <br>
            <span class="cls_004" style="line-height: 20px;">
                <span style="display:inline-block; width: 30px;"></span>{{$body_2}}
            </span>
            <br>
            <br>
            <div style="float: right;text-align: center">
                <span class="cls_005">CAMAT CIMAHI SELATAN,</span>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <span class="cls_005">TUTI HESTIANTINA, S.IP</span>
                <br><br>
                <span>Pembina</span>
                <br><br>
                <span>NIP. 19651004 198603 2 020</span>
            </div>
        </div>
        <div style="position:absolute;left:71.00px;top:779.60px" class="cls_004"><span class="cls_004">Tembusan :</span></div>
        <div style="position:absolute;left:71.00px;top:793.40px" class="cls_004"><span class="cls_004">Lurah Kelurahan
                {{$kelurahan}} Kecamatan Cimahi Selatan</span></div>
    </div>
    <div class="page-break" style="position:absolute;left:50%;margin-left:-307px;top:-42px;width:610px;height:936px;border-style:outset;overflow:hidden">
        <div style="position:absolute;left:0px;top:0px">
            <img src="./img/surat/background2.jpg" width=610 height=936></div>
        <div style="position:absolute;left:310.20px;top:38.60px" class="cls_004"><span class="cls_004">Lampiran Surat</span></div>
        <div style="position:absolute;left:310.20px;top:52.40px" class="cls_004"><span class="cls_004">Nomor</span></div>
        <div style="position:absolute;left:362.82px;top:52.40px" class="cls_004"><span class="cls_004">:
                490/{{$nomor_surat}}/Sapras </span></div>
        <div style="position:absolute;left:310.20px;top:66.20px" class="cls_004"><span class="cls_004">Tanggal</span></div>
        <div style="position:absolute;left:362.22px;top:66.20px" class="cls_004"><span class="cls_004">:
                {{$date}} </span></div>
        <div style="position:absolute;left:71.00px;top:107.60px" class="cls_005">
            <span class="cls_005">Lokasi</span>
            <br>
            <img style="margin-top: 5px" src="{{$longlat}}" alt="">
        </div>
        <div style="position:absolute;left:71.00px;top:356.00px" class="cls_005">
            <span class="cls_005">Foto Pengaduan</span>
            <br>
            <img style="margin-top: 6px" src="{{$foto_pengaduan}}" width="311" height="198" alt="">
        </div>
        <div style="position:absolute;left:71.00px;top:604.40px" class="cls_005">
            <span class="cls_005">Foto Pemeriksaan Petugas</span>
            <br>
            <img style="margin-top: 7px" src="{{$foto_pemeriksaan}}" width="311" height="198" alt="">
        </div>
    </div>

</body>

</html>
