<!DOCTYPE html>
<!--
* CoreUI - Free Bootstrap Admin Template
* @version v2.1.8
* @link https://coreui.io
* Copyright (c) 2018 creativeLabs Łukasz Holeczek
* Licensed under MIT (https://coreui.io/license)
-->

<html lang="en">

<head>
    <base href="./">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="description" content="Halaman admin untuk manajemen konten aplikasi android 'Laporin'">
    <meta name="author" content="PEMKOT CIMAHI">
    <meta name="keyword" content="pemkot, pemerintah, kota, cimahi, bandung, jawa, barat, layanan, masyarakat, laporin, keluhan, solusi, aplikasi, admin">
    <title>SIKOKOM | Admin</title>
    <!-- Icons-->
    <link rel="stylesheet" href="{{url('css/coreui-icons.min.css')}}">
    <link rel="stylesheet" href="{{url('css/flag-icon.min.css')}}">
    <link rel="stylesheet" href="{{url('css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.css">
    <!-- Main styles for this application-->
    <link rel="stylesheet" href="{{url('css/coreui.min.css')}}">
    <link rel="stylesheet" href="{{url('css/pace.min.css')}}">
    <link rel="stylesheet" href="{{url('css/datatables.min.css')}}">
    <link rel="stylesheet" href="{{url('css/quill.snow.css')}}">
    <link rel="stylesheet" href="{{url('css/style.css')}}">
    <link rel="stylesheet" href="{{url('css/datepicker.min.css')}}">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
        integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    {{--
    <link href="https://cdn.jsdelivr.net/npm/gijgo@1.9.10/css/gijgo.min.css" rel="stylesheet" type="text/css" /> --}}
    <!-- Global site tag (gtag.js) - Google Analytics-->
    <script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-118965717-3"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

      function gtag() {
        dataLayer.push(arguments);
      }
      gtag('js', new Date());
      // Shared ID
      gtag('config', 'UA-118965717-3');
      // Bootstrap ID
      gtag('config', 'UA-118965717-5');
    </script>
</head>

<body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
    <header class="app-header navbar">
        <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand" href="{{url('/')}}">
            SIKOKOM Admin
            {{-- <img class="navbar-brand-full" src="{{url('img/logo-cimahi.png')}}" width="89" height="25" alt="Logo Cimahi">
            --}}
            {{-- <img class="navbar-brand-minimized" src="{{url('img/logo-sikokom.jpeg')}}" width="30" height="30" alt="Logo Cimahi">
            --}}

        </a>
        <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show">
            <span class="navbar-toggler-icon"></span>
        </button>
        <ul class="nav navbar-nav ml-auto mr-2">
            <h3 id="userName"></h3>
            <li class="nav-item dropdown">
                <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                    <img class="img-avatar" src="{{url('img/logo-sikokom.jpeg')}}" alt="admin@bootstrapmaster.com">
                </a>
                <div class="dropdown-menu dropdown-menu-right">
                    <div class="dropdown-header text-center">
                        <strong>Account</strong>
                    </div>
                    <a class="dropdown-item" href="{{url('/')}}" onclick="logout()">
                        <i class="fa fa-lock"></i> Logout</a>
                </div>
            </li>
        </ul>
        {{-- <button class="navbar-toggler aside-menu-toggler d-md-down-none" type="button" data-toggle="aside-menu-lg-show">
            <span class="navbar-toggler-icon"></span>
        </button>
        <button class="navbar-toggler aside-menu-toggler d-lg-none" type="button" data-toggle="aside-menu-show">
            <span class="navbar-toggler-icon"></span>
        </button> --}}
    </header>
    <div class="app-body">
        <div class="sidebar">
            <nav class="sidebar-nav">
                <ul class="nav">
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('/')}}">
                            <i class="nav-icon icon-speedometer"></i> Home

                            {{-- <span class="badge badge-warning">9+</span>
                            <span class="sr-only">laporan baru</span> --}}
                        </a>
                    </li>
                    <li class="nav-title">Laporan</li>
                    <li id="verified" class="nav-item d-none">
                        <a class="nav-link" href="{{url('/data-laporan/verified')}}">
                            <i class="nav-icon icon-check"></i> Verified</a>
                    </li>
                    <li class="cmt nav-item d-none">
                        <a class="nav-link" href="{{url('/data-laporan/cmt/unverified')}}">
                            <i class="nav-icon icon-question"></i> Unverified</a>
                    </li>
                    <li class="ksi nav-item d-none">
                        <a class="nav-link" href="{{url('/data-laporan/ksi/unverified')}}">
                            <i class="nav-icon icon-question"></i> Unverified</a>
                    </li>
                    <li class="opr nav-item d-none">
                        <a class="nav-link" href="{{url('/data-laporan/opr/unverified')}}">
                            <i class="nav-icon icon-question"></i> Unverified</a>
                    </li>
                    <li id="rejected" class="nav-item d-none">
                        <a class="nav-link" href="{{url('/data-laporan/rejected')}}">
                            <i class="nav-icon icon-close"></i> Rejected</a>
                    </li>
                    <li class="nav-title">Manajemen</li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('/data-fasilitas')}}">
                            <i class="nav-icon icon-list"></i>Data Fasilitas/Kategori</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('/data-berita')}}">
                            <i class="nav-icon icon-book-open"></i>Data Berita</a>
                    </li>
                </ul>
            </nav>
        </div>
        <main class="main">
            <!-- Breadcrumb-->
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="#">Dashboard</a>
                </li>
                @yield('breadcrumb')
            </ol>
            @yield('content')
        </main>
        <aside class="aside-menu">
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#timeline" role="tab">
                        <i class="icon-list"></i>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#messages" role="tab">
                        <i class="icon-speech"></i>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#settings" role="tab">
                        <i class="icon-settings"></i>
                    </a>
                </li>
            </ul>
        </aside>
    </div>
    <footer class="app-footer">
        <div>
            <a href="#">Laporin</a>
            <span>&copy; 2018 Pemkot Cimahi.</span>
        </div>
        <div class="ml-auto">
            <span>Powered by</span>
            <a href="https://coreui.io">CoreUI</a>
        </div>
    </footer>

    @yield('modal')

    <!-- CoreUI and necessary plugins-->
    <script src="{{url('js/pace.min.js')}}"></script>
    <script src="{{url('js/perfect-scrollbar.min.js')}}"></script>

    <script src="{{url('js/jquery-3.3.1.min.js')}}"></script>
    <script src="{{url('js/popper.min.js')}}"></script>
    <script src="{{url('js/bootstrap.min.js')}}"></script>
    <script src="{{url('js/coreui.min.js')}}"></script>
    <script src="{{url('js/datepicker.min.js')}}"></script>

    <script src="{{url('js/datatables.min.js')}}"></script>
    <script src="{{url('js/quill.js')}}"></script>

    {{-- <script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.10/js/gijgo.min.js" type="text/javascript"></script> --}}
    <script src="{{url('js/custom.js')}}"></script>
    <script src="https://www.gstatic.com/firebasejs/5.5.9/firebase.js"></script>
    <script src="{{url('js/firebase.auth.js')}}"></script>
    @yield('script')
</body>

</html>
