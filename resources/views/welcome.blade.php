<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>SIKOKOM</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>

        <script src="{{url('js/jquery-3.3.1.min.js')}}"></script>
        <script src="https://www.gstatic.com/firebasejs/5.5.9/firebase.js"></script>
        <script src="{{url('js/firebase.auth.js')}}"></script>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    Admin Sikokom
                </div>
                
                <div style="margin-bottom: 40px">
                    <img src="{{url('img/logo-sikokom.jpeg')}}" class="img-fluid" style="width:20vw" alt="">
                </div>

                <div class="links">
                    <a class="cmt" style="display:none" href="{{url('/data-laporan/cmt/unverified')}}">Data Laporan</a>
                    <a class="ksi" style="display:none" href="{{url('/data-laporan/ksi/unverified')}}">Data Laporan</a>
                    <a class="opr" style="display:none" href="{{url('/data-laporan/opr/unverified')}}">Data Laporan</a>
                    <a href="{{url('/data-fasilitas')}}">Data Fasilitas</a>
                    <a href="{{url('/data-berita')}}">Data Berita</a>
                </div>
            </div>
        </div>
    </body>
</html>
